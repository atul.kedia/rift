import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import argparse

#from lalinference.rapid_pe import lalsimutils
import RIFT.lalsimutils as lalsimutils
import EOSManager
import lalsimulation as lalsim
#import lalsim_EOS_tools as let
from scipy.integrate import nquad
#import EOS_param as ep
import os
from EOSPlotUtilities import render_eos

matplotlib.rcParams.update({'font.size': 12.0,  'mathtext.fontset': 'stix'})
matplotlib.rcParams['figure.figsize'] = (9.0, 7.0)
matplotlib.rcParams['xtick.labelsize'] = 15.0
matplotlib.rcParams['ytick.labelsize'] = 15.0
matplotlib.rcParams['axes.labelsize'] = 25.0
matplotlib.rcParams['lines.linewidth'] = 2.0
plt.style.use('seaborn-v0_8-whitegrid')

#spec_params = {'gamma1': -0.701593,'gamma2':-0.239194,'gamma3':0.062016,'gamma4':-0.003556}

eoss = np.genfromtxt('/home/atul.kedia/rift-testing/RIT-matters/communications/20221207-ROSKedia-SpectralRepresentation/names_laleos_improvedPBCS_indices_physical_EOS.txt', dtype='str')

def M_1_4():
    print('M_1.4')

for i in range(0,len(eoss[:])):
    try:
        spec_params = {'gamma1': eoss[i][1].astype(float),
                       'gamma2': eoss[i][2].astype(float),
                       'gamma3': eoss[i][3].astype(float),
                       'gamma4': eoss[i][4].astype(float),
                       'p0': 5.3716e32,  # picked from LALSimNeutronStarEOSSpectralDecomposition.c
                       'epsilon0' : 1e14, # 1.1555e35 / c**2? ~ 0.5nsat
                       'xmax' : 50.0}
        neweos = EOSManager.EOSLindblomSpectralSoundSpeedVersusPressure(spec_params = spec_params)
        
        m_r_L_data = EOSManager.make_mr_lambda_lal(neweos.eos, n_bins=200)
        choose_M_R_L = 'M-R' # 'M-R', 'M-L'
        
        if choose_M_R_L == 'M-R':
            plt.plot(m_r_L_data[:,1], m_r_L_data[:,0])
        elif choose_M_R_L == 'M-L':
            plt.plot(m_r_L_data[:,0], m_r_L_data[:,2])
        
    except:
        continue
    #plt.xlabel(xvar)
    #plt.ylabel(yvar)


plt.legend()
plt.show()


