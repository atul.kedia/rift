import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import argparse

#from lalinference.rapid_pe import lalsimutils
import RIFT.lalsimutils as lalsimutils
import EOSManager
import lalsimulation as lalsim
#import lalsim_EOS_tools as let
from scipy.integrate import nquad
#import EOS_param as ep
import os
from EOSPlotUtilities import render_eos
import lal

matplotlib.rcParams.update({'font.size': 12.0,  'mathtext.fontset': 'stix'})
matplotlib.rcParams['figure.figsize'] = (9.0, 7.0)
matplotlib.rcParams['xtick.labelsize'] = 15.0
matplotlib.rcParams['ytick.labelsize'] = 15.0
matplotlib.rcParams['axes.labelsize'] = 25.0
matplotlib.rcParams['lines.linewidth'] = 2.0
plt.style.use('seaborn-v0_8-whitegrid')

DENSITY_CGS_IN_MSQUARED=1000*lal.G_SI/lal.C_SI**2  # g/cm^3 -> 1/m^2 //GRUnits. Multiply by this to convert from CGS -> 1/m^2 units (_geom). lal.G_SI/lal.C_SI**2 takes kg/m^3 -> 1/m^2  ||  https://lscsoft.docs.ligo.org/lalsuite/lalsimulation/_l_a_l_sim_neutron_star_8h_source.html
PRESSURE_CGS_IN_MSQUARED = DENSITY_CGS_IN_MSQUARED/(lal.C_SI*100)**2

#spec_params = {'gamma1': -0.701593,'gamma2':-0.239194,'gamma3':0.062016,'gamma4':-0.003556}

'''
eoss = np.genfromtxt('/home/atul.kedia/rift-testing/RIT-matters/communications/20221207-ROSKedia-SpectralRepresentation/names_laleos_improvedPBCS_indices_physical_EOS.txt', dtype='str')
'''

eoss = np.array([['AP4', '-0.592110', '-0.264530', '0.085421', '-0.008334'],
        ['SLY4', '-0.779775', '-0.144165', '0.054823', '-0.005143'],
        ['PAL6', '-2.336417', '0.961124', '-0.178733', '0.010829'],
        ['AP3', '-0.463822', '-0.466598', '0.147791', '-0.013972'],
        ['MPA1', '-1.247626', '0.207047', '-0.030886', '0.000749'],
        ['WFF1', '0.233772', '-0.764008', '0.193810', '-0.016393']], dtype=str)


for i in range(0,len(eoss[:])):
    spec_params = {'gamma1': eoss[i][1].astype(float),
                   'gamma2': eoss[i][2].astype(float),
                   'gamma3': eoss[i][3].astype(float),
                   'gamma4': eoss[i][4].astype(float),
                   'p0': 5.3716e32,  # picked from LALSimNeutronStarEOSSpectralDecomposition.c
                   'epsilon0' : 1e14, # 1.1555e35 / c**2? ~ 0.5nsat
                   'xmax' : 50.0}
    
    neweos = EOSManager.EOSLindblomSpectralSoundSpeedVersusPressure(spec_params = spec_params)
    
    xvar, yvar='energy_density', 'pressure'
    new_eos_vals = neweos.make_spec_param_eos(xvar=xvar, yvar=yvar)
    #xvar, yvar ='energy_density', 'pressure', 'rest_mass_density', 'sound_speed_over_c', 'pseudo_enthalpy'
    
    #plt.loglog(new_eos_vals[:,0], new_eos_vals[:,1], label = eoss[i][0])
    #plt.xlabel(xvar)
    #plt.ylabel(yvar)
    
    plot_render = render_eos(eos=neweos.eos, xvar='energy_density', yvar='pressure')
    #render_eos variable names: pseudo_enthalpy, rest_mass_density, baryon_density, pressure, energy_density, sound_speed_over_c

'''
eos_names =  ['LALSimNeutronStarEOS_AP4.dat',
              #'LALSimNeutronStarEOS_SLY4.dat',    # Erroneous for unknown reasons
              'LALSimNeutronStarEOS_PAL6.dat',
              'LALSimNeutronStarEOS_AP3.dat',
              'LALSimNeutronStarEOS_MPA1.dat',
              'LALSimNeutronStarEOS_WFF1.dat']


for name in eos_names:
    data = np.loadtxt("/home/atul.kedia/lalsuite_dir/lalsuite/lalsimulation/lib/"+name, delimiter = "\t")
    
    plt.loglog(data[:,1]/DENSITY_CGS_IN_MSQUARED,data[:,0]/PRESSURE_CGS_IN_MSQUARED, label = name+' raw', ls = 'dashed')
'''

plt.legend()
plt.show()


