import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import argparse

#from lalinference.rapid_pe import lalsimutils
import RIFT.lalsimutils as lalsimutils
import EOSManager
import lalsimulation as lalsim
#import lalsim_EOS_tools as let
from scipy.integrate import nquad
#import EOS_param as ep
import os
from EOSPlotUtilities import render_eos
import lal

matplotlib.rcParams.update({'font.size': 12.0,  'mathtext.fontset': 'stix'})
matplotlib.rcParams['axes.unicode_minus'] = False
matplotlib.rcParams['figure.figsize'] = (9.0, 7.0)
matplotlib.rcParams['xtick.labelsize'] = 15.0
matplotlib.rcParams['ytick.labelsize'] = 15.0
matplotlib.rcParams['axes.labelsize'] = 25.0
matplotlib.rcParams['lines.linewidth'] = 2.0
plt.style.use('seaborn-v0_8-whitegrid')
fig,(ax1,ax2) = plt.subplots(2,1)

eos_names =  ['LALSimNeutronStarEOS_AP4.dat',
              'LALSimNeutronStarEOS_PAL6.dat',
              'LALSimNeutronStarEOS_AP3.dat',
              'LALSimNeutronStarEOS_MPA1.dat',
              'LALSimNeutronStarEOS_WFF1.dat',
              'LALSimNeutronStarEOS_GNH3.dat']

import pyreprimand as pyr

for name in eos_names:
    print(name)
    data = np.loadtxt("/home/atul.kedia/lalsuite_dir/lalsuite/lalsimulation/lib/"+name, delimiter = "\t")
    '''
    neweos = EOSManager.EOSFromTabularData(eos_data=data)
    
    min_pseudo_enthalpy = 0.005
    max_pseudo_enthalpy = lalsim.SimNeutronStarEOSMaxPseudoEnthalpy(neweos.eos)
    hvals = max_pseudo_enthalpy* 10**np.linspace( np.log10(min_pseudo_enthalpy/max_pseudo_enthalpy),  0,num=500)
    qry = EOSManager.QueryLS_EOS(neweos.eos)
    
    param_dict = {'pseudo_enthalpy': hvals,
                  'rest_mass_density': qry.extract_param('rest_mass_density',hvals),
                  'energy_density':qry.extract_param('energy_density',hvals),
                  'pressure':qry.extract_param('pressure',hvals),
                  'sound_speed_over_c':qry.extract_param('sound_speed_over_c',hvals)
                  }
    
    param_dict = EOSManager.eos_monotonic_parts_and_causal_sound_speed(param_dict, preserve_same_length = True)
    
    new_eos = EOSManager.EOSReprimand(param_dict=param_dict)
    '''
    new_eos = EOSManager.EOSReprimand(load_eos=data)
    
    ax1.plot(new_eos._pyr_mrL_dat[:,1], new_eos._pyr_mrL_dat[:,0], label = name)
    ax1.set_xlabel(r"$R\,[\mathrm{km}]$")
    ax1.set_ylabel(r"$M \,[M_\odot]$")
    
    ax2.plot(new_eos._pyr_mrL_dat[:,0], new_eos._pyr_mrL_dat[:,2], label = name)
    ax2.set_xlabel(r"$M [M_\odot]$")
    ax2.set_ylabel(r"$\tilde{\Lambda}$")
    ax2.set_xlim(0.7,2.5)
    ax2.set_ylim(30,1000)

plt.title('M-R using RePrimAnd')
plt.legend()
plt.show()

