This document details the upgrades to EOSManager, made so that RIFT has a better EOS handling capability, and is able to use tabular, causal-sound speed Spectral representation, general LALSimulation EOS objects for EOS goals and includes an interface with RePrimAnd ( https://wokast.github.io/RePrimAnd/tov_solver.html ) for TOV sequences (Mass-Radius-Tidal deformability relations).

Changes made: 4 classes added, 0 removed. 4 methods added, 0 removed. No changes in the previously present classes and methods. 

The update can be viewed in : `https://git.ligo.org/atul.kedia/rift/-/tree/master/MonteCarloMarginalizeCode/Code/RIFT/physics`
which contains two files of relevance. `EOSManager_old.py` and `EOSManager.py` . The first is a copy of the original EOSManager.py (only needed for review purposes). The second is the new EOSManager.py.
Further, test files by the name `demo_new_spectral.py`, `demo_mass_radius.py`,  `EOSPlotUtilities.py`, `demo_eos_tabular.py`, `demo_reprimand.py`, and `demo_reprimand_class.py` have been added for demonstration purposes (only needed for review purposes). This update also requires the use of updated causal spectral lalsimulation detailed in `https://git.ligo.org/leslie.wade/lalsuite/-/blob/eos-patch/lalsimulation/lib/LALSimNeutronStarEOSSpectralDecomposition.c` and requires `RePrimAnd` (v1.6 used for testing purposes). Currently, Q1 of 2023, this updated lalsimulation is being reviewed for merger into the master branch of lalsim.

The updates to EOSManager are as follows:

Line  232- 288: `class EOSFromTabularData`

'''

Input: 
       * Tabular data (baryon_density = n , pressure = p, energy density = \rho)
       * method for primitives: this information is partially redundant, in that \ln n_b/n_ref = \int   c^2 [d rho] / (P(rho) + rho c^2), etc
          Need some specific choice for inter-edge interpolation (and redundance resolution) 
       * Low-density approximation (if desired) for filling down to surface density.  
           WARNING: Will generally match imperfectly, need some resolution to that procedure
    Creates
        * LALSimulation data structure as desired
    Warning: 
        * Currently generates intermediate data file by writing to disk

'''

Reads tabulated data and constructs EOS object. Code for EOS table format is at `https://github.com/oshaughn/RIT-matters/blob/master/communications/20221207-ROSKedia-SpectralRepresentation/convert_compose_EOS.py#L115` , for example `eos_manager.txt` is attached in this repo. The three columns are 'baryon_density', 'pressure', 'energy_density' in cgs units. This can also accept LALSim table format, such as given in `lalsuite/lalsimulation/lib/LALSimNeutronStarEOS_AP4.dat` ,i.e., two columns 'pressure' and 'energy_density' in geometerized units. Also implemented are tests for phase transitions that cause non-monotonicity in P v E. An artificially constructed phase transition eos sample is `eos_manager_phase_transition.txt` is attached in this repo.

Code to test `demo_eos_tabular.py`.


Line  699- 685: `class EOSLindblomSpectralSoundSpeedVersusPressure`

'''

Based on Lindblom (2022) https://journals.aps.org/prd/abstract/10.1103/PhysRevD.105.063031  <-> https://arxiv.org/pdf/2202.12285.pdf   
    EOS spectral representation of sound speed versus pressure, as expansion of Upsilon(p): see Eq. (11).
    Uses function call to lalsuite to implement low-level interface

'''

Constructs EOS object using four indices (for simplicity are labelled same as `EOSLindblomSpectral`, i.e., gamma1,2,3,4) and can return relevant thermodynamic quantities as QueryLS_EOS.

Code to test `demo_new_spectral.py`.

Line 1045-1086: `class QueryLS_EOS`

'''

Class to repeatedly query a single lalsimulation EOS object, using a common interface (e.g., to extract array outputs by name, unit conversions, etc).

'''

Uses LALSim functions to construct EOS object. The class can return various thermodynamic quantities and perform unit conversions from cgs to MeV units.

Code to test `demo_new_spectral.py` along with `EOSPlotUtilities.py`.

Units: For converting between CGS-Geometerized(everthing in 'meters')-SI we use the following conversion scheme (also noted under EOSManager.EOSFromTabularData):

Use https://www.seas.upenn.edu/~amyers/NaturalUnits.pdf for reference
Convert Pressure in CGS to SI.
Ba -> Pa is a factor of 0.1 because [Ba] = g/(cm s^2) = 0.1 kg/(m s^2) = 0.1 Pa

Convert Density in CGS-mass density [Mass/Volume] to SI-energy density [Energy/Volume].
Converts CGS -> SI, i.e., mass density units to energy density units g/cm^3 -> J/m^3. 
Steps: 1 g/cm^3 -> 1000 kg/m^3 . Now multiply by c^2 to get 1000kg/m^3 * c^2 = 1000*lal.C_SI^2 J/m^3. 
OR Steps:  1 g/cm^3 multiplied by c^2 to get 1 g/cm^3 * c^2 = (lal.C_SI*100)^2 (g cm^2/s^2)/cm^3 = (lal.C_SI*100)^2 erg/cm^3 = (lal.C_SI*100)^2 *0.1 J/m^3. QED.

Convert Pressure in CGS to Geometerized Units.
First Convert Pressure in CGS to SI units. I.e.,
Ba = 0.1 Pa
Then to go from Pa = kg/(m s^2) to 1/m^2 multiply by lal.G_SI/lal.C_SI^4
Hence, to go from Ba to 1/m^2, multiply by 0.1 lal.G_SI/lal.C_SI^4, or DENSITY_CGS_IN_MSQUARED/(lal.C_SI*100)**2 = 1000*lal.G_SI/lal.C_SI**2/(lal.C_SI*100)**2

Convert Density in CGS to Geometerized Units
First convert CGS-mass density to  SI-energy density as above:
1 g/cm^3 -> 1000*lal.C_SI^2 J/m^3
Then to go from J/m^3 = kg/(m s^2) to 1/m^2 multiply by lal.G_SI/lal.C_SI^4
Hence, to go from g/cm^3 to 1/m^2, multiply by 1000 lal.G_SI/lal.C_SI^2


MORE CLASSES AND METHODS ADDED 
methods: `check_sound_speed_causal`, `check_monotonic`, `eos_monotonic_parts_and_causal_sound_speed` and `make_mr_lambda_reprimand`
class: `EOSReprimand`

`class EOSReprimand`

"""Pass param_dict as the dictionary of 'pseudo_enthalpy','rest_mass_density','energy_density','pressure','sound_speed_over_c' for being resolved into a TOV sequence. CGS Units only except sound_speed_over_c.
Instead you can send a lalsim_eos which processes lalsim eos object type and produces a TOV sequence.
load_eos takes a 2D array with pressure, energy_density and rest_mass_density (not tested)"""

Constructs TOV sequence for EOSs supplied. Can send in EOSs in the form of a dictionary `param_dict = {'pseudo_enthalpy': p_enthalpy,'rest_mass_density': rho,'energy_density': edens,'pressure': press,'sound_speed_over_c': cs}` , a LALSim EOS object `lalsim_eos = eos_object` , or a tabular format (pending implementation). Test codes `demo_reprimand.py` and `demo_reprimand_class.py`

`def make_mr_lambda_reprimand`:

"""
Construct mass-radius curve from EOS using RePrimAnd (https://wokast.github.io/RePrimAnd/tov_solver.html).
Parameter `eos` should be in RePrimAnd's eos object format, made with something like `make_eos_barotr_spline` (https://wokast.github.io/RePrimAnd/eos_barotr_ref.html).
"""

Calculates the TOV sequence (M-R-Lambda) for a given EOS. EOS supplied should be in RePrimAnd format.

`def check_monotonic` : 

"""
Checks monotonicity of monotonic_params and removes non-monotonic parts in it for both monotonic_params and other_params.
By default this will reduce the length of data due to deletion of non-monotonic patches, but interpolate_to_same_length can be turned true to keep length of data intact.
"""

Often needed for EOS construction in both RePrimAnd and LALSim.

`def check_sound_speed_causal`:

"""Checks if sound speed exceeds 1 anywhere, and depending on option `truncate` removes that region or or forces it =1. """

Definitely needed for RePrimAnd and may be needed for other analyses.

`def eos_monotonic_parts_and_causal_sound_speed`:

Calls both `check_monotonic` and `check_sound_speed_causal` for ensuring the 'goodness' of the EOS.




