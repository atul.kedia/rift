#
# GOAL
#    Library to facilitate making various plots of equation-of-state-related quantities.


# from matplotlib import pyplot as plt
# import EOSManager
# import EOSPlotUtilities
# my_eos = EOSManager.EOSLALSimulation('SLy')
# EOSPlotUtilities.render_eos(my_eos.eos,'rest_mass_density', 'pressure')

import numpy as np
import EOSManager
import lalsimulation as lalsim
import lal
import matplotlib
import matplotlib.pyplot as plt


matplotlib.rcParams.update({'font.size': 12.0,  'mathtext.fontset': 'stix'})
matplotlib.rcParams['figure.figsize'] = (9.0, 7.0)
matplotlib.rcParams['xtick.labelsize'] = 15.0
matplotlib.rcParams['ytick.labelsize'] = 15.0
matplotlib.rcParams['axes.labelsize'] = 25.0
matplotlib.rcParams['lines.linewidth'] = 2.0
plt.style.use('seaborn-v0_8-whitegrid')


def render_eos(eos, xvar='energy_density', yvar='pressure',units='cgs',npts=100,label=None):
    
    min_pseudo_enthalpy = 0.005
    max_pseudo_enthalpy = lalsim.SimNeutronStarEOSMaxPseudoEnthalpy(eos)
    hvals = max_pseudo_enthalpy* 10**np.linspace( np.log10(min_pseudo_enthalpy/max_pseudo_enthalpy),  0,num=npts)
    
    print(hvals,min_pseudo_enthalpy, max_pseudo_enthalpy)
    
    qry = EOSManager.QueryLS_EOS(eos)
    
    xvals = qry.extract_param(xvar,hvals)
    yvals = qry.extract_param(yvar,hvals)
    print(np.c_[xvals,yvals])
    #xvals = qry.convert(xvals, 'rest_mass_density') # using QueryLS_EOS.convert
    
    plt.loglog(xvals, yvals)
    plt.scatter(xvals, yvals)
    plt.xlabel(xvar)
    plt.ylabel(yvar)
    
    return None




