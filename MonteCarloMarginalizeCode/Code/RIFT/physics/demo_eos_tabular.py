import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import argparse

#from lalinference.rapid_pe import lalsimutils
import RIFT.lalsimutils as lalsimutils
import EOSManager
import lalsimulation as lalsim
#import lalsim_EOS_tools as let
from scipy.integrate import nquad
#import EOS_param as ep
import os
from EOSPlotUtilities import render_eos
import lal

matplotlib.rcParams.update({'font.size': 12.0,  'mathtext.fontset': 'stix'})
matplotlib.rcParams['figure.figsize'] = (9.0, 7.0)
matplotlib.rcParams['xtick.labelsize'] = 15.0
matplotlib.rcParams['ytick.labelsize'] = 15.0
matplotlib.rcParams['axes.labelsize'] = 25.0
matplotlib.rcParams['lines.linewidth'] = 2.0
plt.style.use('seaborn-v0_8-whitegrid')

DENSITY_CGS_IN_MSQUARED=1000*lal.G_SI/lal.C_SI**2  # g/cm^3 -> 1/m^2 //GRUnits. Multiply by this to convert from CGS -> 1/m^2 units (_geom). lal.G_SI/lal.C_SI**2 takes kg/m^3 -> 1/m^2  ||  https://lscsoft.docs.ligo.org/lalsuite/lalsimulation/_l_a_l_sim_neutron_star_8h_source.html
PRESSURE_CGS_IN_MSQUARED = DENSITY_CGS_IN_MSQUARED/(lal.C_SI*100)**2



data = np.genfromtxt("eos_manager_phase_transition.txt", delimiter = " ", names=True, dtype=None)
#data = np.genfromtxt("eos_manager_phase_transition.txt", delimiter = " ", names=True, dtype=None)


neweos = EOSManager.EOSFromTabularData(eos_data=data)

plt.loglog(data['energy_density']/DENSITY_CGS_IN_MSQUARED,data['pressure']/PRESSURE_CGS_IN_MSQUARED, label = ' raw')
plt.scatter(data['energy_density']/DENSITY_CGS_IN_MSQUARED,data['pressure']/PRESSURE_CGS_IN_MSQUARED, label = ' raw')

plot_render = render_eos(eos=neweos.eos, xvar='energy_density', yvar='pressure')

'''

eos_names =  ['LALSimNeutronStarEOS_AP4.dat',
              #'LALSimNeutronStarEOS_SLY4.dat',    # Erroneous for unknown reasons
              'LALSimNeutronStarEOS_PAL6.dat',
              'LALSimNeutronStarEOS_AP3.dat',
              'LALSimNeutronStarEOS_MPA1.dat',
              'LALSimNeutronStarEOS_WFF1.dat']


for name in eos_names:
    data = np.loadtxt("/home/atul.kedia/lalsuite_dir/lalsuite/lalsimulation/lib/"+name, delimiter = "\t")
    
    neweos = EOSManager.EOSFromTabularData(eos_data=data)
    
    plt.loglog(data[:,1]/DENSITY_CGS_IN_MSQUARED,data[:,0]/PRESSURE_CGS_IN_MSQUARED, label = name+' raw', ls = 'dashed')
    plot_render = render_eos(eos=neweos.eos, xvar='energy_density', yvar='pressure')
'''

plt.show()

